package com.msds.service.repository

import com.msds.service.model.User
import kotlinx.coroutines.tasks.await

object UserRepository {
    suspend fun addUser(userId: String, user: User) {
        FirebaseService.usersCollection.document(userId).set(user).await()
    }

    suspend fun getUser(userId: String): User? {
        return FirebaseService.usersCollection
            .document(userId).get().await().toObject(User::class.java)
    }

    suspend fun isUserExist(userId: String): Boolean {
        return FirebaseService.usersCollection.document(userId).get().await().exists()
    }

    suspend fun removeUser(userId: String) {
        FirebaseService.usersCollection.document(userId).delete().await()
    }

    suspend fun updateUser(userId: String, user: User) {
        FirebaseService.usersCollection.document(userId).set(user).await()
    }
}