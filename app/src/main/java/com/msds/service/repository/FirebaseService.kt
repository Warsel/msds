package com.msds.service.repository

import com.google.firebase.firestore.FirebaseFirestore

object FirebaseService {
    private val database: FirebaseFirestore = FirebaseFirestore.getInstance()

    val usersCollection = database.collection("users")
}