package com.msds.service.analysis

interface IDataAnalysis {
    fun addPoints(data: ByteArray)
    fun save(workPlaceIndex: Int)
    fun startCurrentValueUpdateTimer()
    fun stopCurrentValueUpdateTimer()
}