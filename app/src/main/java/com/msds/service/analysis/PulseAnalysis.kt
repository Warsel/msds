package com.msds.service.analysis

import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import com.msds.service.GlobalState
import com.msds.service.model.ChannelNumber
import com.msds.service.model.ChannelProperties
import com.msds.service.model.PulseAnalysisProperties
import com.msds.service.model.WorkPlace
import com.msds.service.repository.UserRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlin.math.roundToInt

class PulseAnalysis(
    private val firstChannelSeries: LineGraphSeries<DataPoint>,
    private val secondChannelSeries: LineGraphSeries<DataPoint>,
    private val selectedOrganizationIndex: Int,
    private val selectedFarmIndex: Int,
    private val channelsValuesUpdateEventListener: IChannelsValuesUpdateEventListener
) : IDataAnalysis {
    private val _firstChannelProperties = ChannelProperties()
    private val _secondChannelProperties = ChannelProperties()
    private var _firstChannelPropertiesPreview = ChannelProperties()
    private var _secondChannelPropertiesPreview = ChannelProperties()

    private var _xAxisValue = 0.0

    override fun addPoints(data: ByteArray) {
        var firstChannelVacuumValue: Double
        var secondChannelVacuumValue: Double
        var firstChannelIndex = 0
        var secondChannelIndex = 2

        for (i in 0..4) {
            firstChannelVacuumValue = (data[firstChannelIndex + 1].toInt() and 0xff shl 8 or (data[firstChannelIndex].toInt() and 0xff)).toDouble()
            firstChannelIndex += 4
            secondChannelVacuumValue = (data[secondChannelIndex + 1].toInt() and 0xff shl 8 or (data[secondChannelIndex].toInt() and 0xff)).toDouble()
            secondChannelIndex += 4

            firstChannelVacuumValue *= DeviceSettings.ADC_REQUIRED_FIRST_CHANNEL / DeviceSettings.ADC_VALUE_FIRST_CHANNEL
            secondChannelVacuumValue *= DeviceSettings.ADC_REQUIRED_SECOND_CHANNEL / DeviceSettings.ADC_VALUE_SECOND_CHANNEL

            firstChannelSeries.appendData(DataPoint(_xAxisValue, firstChannelVacuumValue), true, 1250)
            secondChannelSeries.appendData(DataPoint(_xAxisValue, secondChannelVacuumValue), true, 1250)

            findFluctuations(firstChannelVacuumValue, _firstChannelProperties, ChannelNumber.FIRST)
            findFluctuations(secondChannelVacuumValue, _secondChannelProperties, ChannelNumber.SECOND)

            _xAxisValue += 1
        }
    }

    override fun save(workPlaceIndex: Int) {
        val firstChannelProperties = PulseAnalysisProperties()
        firstChannelProperties.pulseCount = _firstChannelPropertiesPreview.pulseCount
        firstChannelProperties.aPhase = _firstChannelPropertiesPreview.aPhase
        firstChannelProperties.bPhase = _firstChannelPropertiesPreview.bPhase
        firstChannelProperties.cPhase = _firstChannelPropertiesPreview.cPhase
        firstChannelProperties.dPhase = _firstChannelPropertiesPreview.dPhase
        firstChannelProperties.ePhase = _firstChannelPropertiesPreview.ePhase
        firstChannelProperties.fPhase = _firstChannelPropertiesPreview.fPhase
        firstChannelProperties.minVacuum = _firstChannelPropertiesPreview.minVacuum
        firstChannelProperties.maxVacuum = _firstChannelPropertiesPreview.maxVacuum
        firstChannelProperties.points = _firstChannelPropertiesPreview.lastBuffer
        firstChannelProperties.aPhaseIndex = _firstChannelPropertiesPreview.aPhaseIndex
        firstChannelProperties.bPhaseIndex = _firstChannelPropertiesPreview.bPhaseIndex
        firstChannelProperties.cPhaseIndex = _firstChannelPropertiesPreview.cPhaseIndex
        firstChannelProperties.dPhaseIndex = _firstChannelPropertiesPreview.dPhaseIndex

        val secondChannelProperties = PulseAnalysisProperties()
        secondChannelProperties.pulseCount = _secondChannelPropertiesPreview.pulseCount
        secondChannelProperties.aPhase = _secondChannelPropertiesPreview.aPhase
        secondChannelProperties.bPhase = _secondChannelPropertiesPreview.bPhase
        secondChannelProperties.cPhase = _secondChannelPropertiesPreview.cPhase
        secondChannelProperties.dPhase = _secondChannelPropertiesPreview.dPhase
        secondChannelProperties.ePhase = _secondChannelPropertiesPreview.ePhase
        secondChannelProperties.fPhase = _secondChannelPropertiesPreview.fPhase
        secondChannelProperties.minVacuum = _secondChannelPropertiesPreview.minVacuum
        secondChannelProperties.maxVacuum = _secondChannelPropertiesPreview.maxVacuum
        secondChannelProperties.points = _secondChannelPropertiesPreview.lastBuffer
        secondChannelProperties.aPhaseIndex = _secondChannelPropertiesPreview.aPhaseIndex
        secondChannelProperties.bPhaseIndex = _secondChannelPropertiesPreview.bPhaseIndex
        secondChannelProperties.cPhaseIndex = _secondChannelPropertiesPreview.cPhaseIndex
        secondChannelProperties.dPhaseIndex = _secondChannelPropertiesPreview.dPhaseIndex

        if (GlobalState.user!!.organizations[selectedOrganizationIndex].farms[selectedFarmIndex].workplaces.size > workPlaceIndex) {
            GlobalState.user!!.organizations[selectedOrganizationIndex].farms[selectedFarmIndex].workplaces[workPlaceIndex].firstChannelProperties = firstChannelProperties
            GlobalState.user!!.organizations[selectedOrganizationIndex].farms[selectedFarmIndex].workplaces[workPlaceIndex].secondChannelProperties = secondChannelProperties
        } else {
            GlobalState.user!!.organizations[selectedOrganizationIndex].farms[selectedFarmIndex].workplaces.add(
                WorkPlace(workPlaceIndex.toString(), firstChannelProperties, secondChannelProperties)
            )
        }

        GlobalScope.launch {
            UserRepository.updateUser(GlobalState.userId!!, GlobalState.user!!)
        }
    }

    override fun startCurrentValueUpdateTimer() { }

    override fun stopCurrentValueUpdateTimer() { }

    private fun findFluctuations(vacuumValue: Double, channelProperties: ChannelProperties, channelNumber: ChannelNumber) {
        if (vacuumValue > channelProperties.maxVacuum) {
            channelProperties.maxVacuum = vacuumValue
        }

        if (vacuumValue < channelProperties.minVacuum) {
            channelProperties.minVacuum = vacuumValue
        }

        if (!channelProperties.flags.pulseBegun) {
            channelProperties.preBuffer.add(vacuumValue)

            if (channelProperties.preBuffer.size > DeviceSettings.PRE_BUFFER_SIZE) {
                channelProperties.preBuffer.removeAt(0)
            }
        }

        channelProperties.buffer.add(vacuumValue)

        if (!channelProperties.flags.pulseBegun) {
            channelProperties.flags.pulseBegun = channelProperties.applyPhaseFilter(vacuumValue, DeviceSettings.BORDER_POINT, true)  // border point
            if (channelProperties.flags.pulseBegun) {
                channelProperties.resetPhaseFilter()
                channelProperties.aPhase = getPhaseValue(channelProperties.aPhaseIndex, channelProperties.bPhaseIndex).toDouble()
                channelProperties.bPhase = getPhaseValue(channelProperties.bPhaseIndex, channelProperties.cPhaseIndex).toDouble()
                channelProperties.cPhase = getPhaseValue(channelProperties.cPhaseIndex, channelProperties.dPhaseIndex).toDouble()
                channelProperties.dPhase = getPhaseValue(channelProperties.dPhaseIndex, channelProperties.buffer.size).toDouble()
                channelProperties.ePhase = (channelProperties.aPhase + channelProperties.bPhase)
                channelProperties.fPhase = channelProperties.cPhase + channelProperties.dPhase
                channelProperties.pulseCount = (1000.0 - (channelProperties.fPhase + channelProperties.ePhase)) / (channelProperties.fPhase + channelProperties.ePhase) * 60 + 60

                channelsValuesUpdateEventListener.onChannelPropertiesUpdate(
                    channelProperties.maxVacuum.roundToInt().toString(),
                    channelProperties.minVacuum.roundToInt().toString(),
                    channelProperties.pulseCount.roundToInt().toString(),
                    channelProperties.ePhase.roundToInt().toString(),
                    channelProperties.fPhase.roundToInt().toString(),
                    channelNumber
                )

                channelProperties.lastBuffer.clear()
                channelProperties.lastBuffer.addAll(channelProperties.buffer)

                if (channelNumber == ChannelNumber.FIRST) {
                    _firstChannelPropertiesPreview = channelProperties.clone()
                } else {
                    _secondChannelPropertiesPreview = channelProperties.clone()
                }

                channelProperties.resetProperties()
            }
        }

        if (channelProperties.flags.pulseBegun) {
            if (!channelProperties.flags.phaseCFound) {
                channelProperties.flags.phaseCFound = channelProperties.applyPhaseFilter(vacuumValue, channelProperties.maxVacuum - DeviceSettings.BORDER_POINT, false)

                if (channelProperties.flags.phaseCFound) {
                    channelProperties.cPhaseIndex = channelProperties.buffer.size
                    channelProperties.resetPhaseFilter()

                    for (i: Int in 0 until channelProperties.buffer.size) {
                        if (channelProperties.applyPhaseFilter(channelProperties.buffer[i], channelProperties.maxVacuum - DeviceSettings.BORDER_POINT, true)) {
                            channelProperties.bPhaseIndex = i

                            break
                        }
                    }

                    channelProperties.resetPhaseFilter()
                    return
                }
            }

            if (channelProperties.flags.phaseCFound && !channelProperties.flags.phaseDFound) {
                channelProperties.flags.phaseDFound = channelProperties.applyPhaseFilter(vacuumValue, DeviceSettings.BORDER_POINT, false)

                if (channelProperties.flags.phaseDFound) {
                    channelProperties.resetPhaseFilter()

                    channelProperties.dPhaseIndex = channelProperties.buffer.size

                    channelProperties.pulseCount += 1
                    channelProperties.flags.pulseBegun = false

                    val preBufferSize = channelProperties.preBuffer.size
                    channelProperties.shiftPhasesIndexes(preBufferSize)

                    channelProperties.buffer.addAll(0, channelProperties.preBuffer)
                    channelProperties.preBuffer.clear()
                }
            }
        }
    }

    private fun getPhaseValue(firstIndex: Int, secondIndex: Int): Int {
        return ((secondIndex - firstIndex).toDouble() * (1.0 / DeviceSettings.POINTS_PER_SECOND) * 1000.0).roundToInt()  // 600 количесвто точек
    }
}