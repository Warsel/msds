package com.msds.service.analysis

import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import com.msds.service.model.ChannelNumber
import com.msds.service.model.ManometerProperties
import java.util.*
import kotlin.concurrent.timer

class ManometerAnalysis(
    private val firstChannelSeries: LineGraphSeries<DataPoint>,
    private val secondChannelSeries: LineGraphSeries<DataPoint>,
    private val channelsValuesUpdateEventListener: IChannelsValuesUpdateEventListener
) : IDataAnalysis {
    private var _firstChannelProperties = ManometerProperties()
    private var _secondChannelProperties = ManometerProperties()

    private var _xAxisValue = 0.0

    private var _currentValueUpdateTimer: Timer? = null

    override fun addPoints(data: ByteArray) {
        var firstChannelVacuumValue: Int
        var secondChannelVacuumValue: Int
        var firstChannelIndex = 0
        var secondChannelIndex = 2

        for (i in 0..4) {
            firstChannelVacuumValue = data[firstChannelIndex + 1].toInt() and 0xff shl 8 or (data[firstChannelIndex].toInt() and 0xff)
            firstChannelIndex += 4
            secondChannelVacuumValue = data[secondChannelIndex + 1].toInt() and 0xff shl 8 or (data[secondChannelIndex].toInt() and 0xff)
            secondChannelIndex += 4

            if (firstChannelVacuumValue > _firstChannelProperties.maxVacuumValue) {
                _firstChannelProperties.maxVacuumValue = firstChannelVacuumValue
                channelsValuesUpdateEventListener.onChannelMaxVacuumValueUpdate(firstChannelVacuumValue.toString(), ChannelNumber.FIRST)
            }
            if (firstChannelVacuumValue < _firstChannelProperties.minVacuumValue) {
                _firstChannelProperties.minVacuumValue = firstChannelVacuumValue
                channelsValuesUpdateEventListener.onChannelMinVacuumValueUpdate(firstChannelVacuumValue.toString(), ChannelNumber.FIRST)
            }
            if (secondChannelVacuumValue > _secondChannelProperties.maxVacuumValue) {
                _secondChannelProperties.maxVacuumValue = secondChannelVacuumValue
                channelsValuesUpdateEventListener.onChannelMaxVacuumValueUpdate(secondChannelVacuumValue.toString(), ChannelNumber.SECOND)
            }
            if (secondChannelVacuumValue < _secondChannelProperties.minVacuumValue) {
                _secondChannelProperties.minVacuumValue = secondChannelVacuumValue
                channelsValuesUpdateEventListener.onChannelMinVacuumValueUpdate(secondChannelVacuumValue.toString(), ChannelNumber.SECOND)
            }

            firstChannelSeries.appendData(DataPoint(_xAxisValue, firstChannelVacuumValue.toDouble()), true, 1250)
            secondChannelSeries.appendData(DataPoint(_xAxisValue, secondChannelVacuumValue.toDouble()), true, 1250)

            if (i == 4) {
                _firstChannelProperties.currentVacuumValue = firstChannelVacuumValue
                _secondChannelProperties.currentVacuumValue = secondChannelVacuumValue
            }

            _xAxisValue += 1
        }
    }

    override fun save(workPlaceIndex: Int) {

    }

    override fun startCurrentValueUpdateTimer() {
        _currentValueUpdateTimer = timer(
            null,
            false,
            100,
            250
        ) {
            channelsValuesUpdateEventListener.onChannelCurrentVacuumValueUpdate(_firstChannelProperties.currentVacuumValue.toString(), ChannelNumber.FIRST)
            channelsValuesUpdateEventListener.onChannelCurrentVacuumValueUpdate(_secondChannelProperties.currentVacuumValue.toString(), ChannelNumber.SECOND)
        }
    }

    override fun stopCurrentValueUpdateTimer() {
        _currentValueUpdateTimer?.cancel()
    }
}