package com.msds.service.analysis

import com.msds.service.model.ChannelNumber

interface IChannelsValuesUpdateEventListener {
    fun onChannelMaxVacuumValueUpdate(value: String, channelNumber: ChannelNumber)

    fun onChannelMinVacuumValueUpdate(value: String, channelNumber: ChannelNumber)

    fun onChannelCurrentVacuumValueUpdate(value: String, channelNumber: ChannelNumber)

    fun onChannelPropertiesUpdate(
        maxVacuum: String,
        minVacuum: String,
        pulseCount: String,
        ePhase: String,
        fPhase: String,
        channelNumber: ChannelNumber
    )
}