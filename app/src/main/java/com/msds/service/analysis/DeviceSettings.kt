package com.msds.service.analysis

object DeviceSettings {
    const val POINTS_PER_SECOND = 600.0
    var PRE_BUFFER_SIZE = 100
    var BORDER_POINT = 50.0
    var VACUUM_MAX_VALUE = 1000.0
    var ADC_VALUE_FIRST_CHANNEL = 4095.0
    var ADC_REQUIRED_FIRST_CHANNEL = 4095.0
    var ADC_VALUE_SECOND_CHANNEL = 4095.0
    var ADC_REQUIRED_SECOND_CHANNEL = 4095.0
}