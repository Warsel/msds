package com.msds.service.model

enum class ChannelNumber {
    FIRST,
    SECOND
}