package com.msds.service.model

class Organization(
    var name: String,
    val farms: MutableList<Farm> = mutableListOf()
) {
    constructor(): this("")
}