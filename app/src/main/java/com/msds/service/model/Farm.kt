package com.msds.service.model

class Farm(
    var name: String,
    val workplaces: MutableList<WorkPlace> = mutableListOf()
) {
    constructor(): this("")
}