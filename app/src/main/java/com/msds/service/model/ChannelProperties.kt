package com.msds.service.model

import com.google.gson.Gson

class ChannelProperties(
    val preBuffer: ArrayList<Double> = ArrayList(),
    val buffer: ArrayList<Double> = ArrayList(),
    var minVacuum: Double = Double.MAX_VALUE,
    var maxVacuum: Double = Double.MIN_VALUE,
    var aPhase: Double = 0.0,
    var bPhase: Double = 0.0,
    var cPhase: Double = 0.0,
    var dPhase: Double = 0.0,
    var ePhase: Double = 0.0,
    var fPhase: Double = 0.0,
    var aPhaseIndex: Int = 0,
    var bPhaseIndex: Int = 0,
    var cPhaseIndex: Int = 0,
    var dPhaseIndex: Int = 0,
    var pulseCount: Double = 0.0,
    val flags: PulseAnalysisFlags = PulseAnalysisFlags(),
    val lastBuffer: ArrayList<Double> = ArrayList(),
    private var phaseFilter: Int = 0
    ) {
    fun resetPhaseFilter() {
        phaseFilter = 0
    }

    fun applyPhaseFilter(pointValue: Double, borderPointValue: Double, isRise: Boolean): Boolean {
        if (isRise && pointValue > borderPointValue) {
            return ++phaseFilter == 5
        }

        if (!isRise && pointValue < borderPointValue) {
            return ++phaseFilter == 5
        }

        resetPhaseFilter()
        return false
    }

    fun shiftPhasesIndexes(offsetValue: Int) {
        aPhaseIndex += offsetValue
        bPhaseIndex += offsetValue
        cPhaseIndex += offsetValue
        dPhaseIndex += offsetValue
    }

    fun resetProperties() {
        minVacuum = Double.MAX_VALUE
        maxVacuum = Double.MIN_VALUE
        flags.phaseCFound = false
        flags.phaseDFound = false
        aPhaseIndex = 0
        buffer.clear()
    }

    fun clone(): ChannelProperties {
        val stringProject = Gson().toJson(this, ChannelProperties::class.java)
        return Gson().fromJson(stringProject, ChannelProperties::class.java)
    }
}