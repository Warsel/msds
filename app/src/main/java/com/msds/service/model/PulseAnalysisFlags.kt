package com.msds.service.model

class PulseAnalysisFlags (
    var pulseBegun: Boolean = false,
    var phaseCFound: Boolean = false,
    var phaseDFound: Boolean = false
)