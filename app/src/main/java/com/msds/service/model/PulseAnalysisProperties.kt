package com.msds.service.model

class PulseAnalysisProperties(
    var minVacuum: Double = Double.MAX_VALUE,
    var maxVacuum: Double = Double.MIN_VALUE,
    var pulseCount: Double = 0.0,
    var aPhase: Double = 0.0,
    var bPhase: Double = 0.0,
    var cPhase: Double = 0.0,
    var dPhase: Double = 0.0,
    var ePhase: Double = 0.0,
    var fPhase: Double = 0.0,
    var aPhaseIndex: Int = 0,
    var bPhaseIndex: Int = 0,
    var cPhaseIndex: Int = 0,
    var dPhaseIndex: Int = 0,
    var points: ArrayList<Double> = arrayListOf()
) {
}