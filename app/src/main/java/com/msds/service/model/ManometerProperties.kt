package com.msds.service.model

class ManometerProperties {
    var maxVacuumValue: Int = Int.MIN_VALUE
    var minVacuumValue: Int = Int.MAX_VALUE
    var currentVacuumValue: Int = 0
}