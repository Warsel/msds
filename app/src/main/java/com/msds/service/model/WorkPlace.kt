package com.msds.service.model

class WorkPlace(
    var name: String,
    var firstChannelProperties: PulseAnalysisProperties,
    var secondChannelProperties: PulseAnalysisProperties
) {
    constructor(): this("", PulseAnalysisProperties(), PulseAnalysisProperties())
}