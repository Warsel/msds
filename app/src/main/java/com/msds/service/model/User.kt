package com.msds.service.model

import java.util.*

class User (
    val organizations: MutableList<Organization> = mutableListOf(),
    val creationTime: Long = Date().time
)