package com.msds.service

import com.msds.service.model.User

object GlobalState {
    var userId: String? = null
    var user: User? = null
}