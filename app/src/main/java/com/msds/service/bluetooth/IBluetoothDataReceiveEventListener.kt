package com.msds.service.bluetooth

interface IBluetoothDataReceiveEventListener {
    fun onDataReceive(data: ByteArray)
}