package com.msds.service.bluetooth.request

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.util.Log
import com.msds.service.bluetooth.IBluetoothEventListener

class DiscoverRequest : IBluetoothRequest {
    private var context: Context? = null
    private var eventListener: IBluetoothEventListener? = null
    private val bluetoothAdapter: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

    private var discoveredDevices = ArrayList<BluetoothDevice>()

    fun discover() {
        discoveredDevices.clear()

        if (bluetoothAdapter.isDiscovering) {
            bluetoothAdapter.cancelDiscovery()
        }

        bluetoothAdapter.startDiscovery()
        eventListener!!.onDiscovering()
    }

    private fun addDiscoveredDevice(device: BluetoothDevice) {
        if (device.bondState == BluetoothDevice.BOND_BONDED)
            return

        for (discoveredDevice in discoveredDevices) {
            if (discoveredDevice.address == device.address) {
                return
            }
        }

        discoveredDevices.add(device)
        eventListener!!.onDeviceFound(discoveredDevices)
    }

    private val discoverReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (BluetoothDevice.ACTION_FOUND == intent.action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                device?.apply {
                    addDiscoveredDevice(this)
                }
            }

            if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED == intent.action) {
                eventListener!!.onDiscovered()
            }
        }
    }

    private fun registerReceiver() {
        context!!.registerReceiver(discoverReceiver, IntentFilter(BluetoothDevice.ACTION_FOUND))
        context!!.registerReceiver(discoverReceiver, IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED))
    }

    override fun setContext(context: Context) {
        this.context = context
        registerReceiver()
    }

    override fun setEventListener(bluetoothEventListener: IBluetoothEventListener) {
        this.eventListener = bluetoothEventListener
    }

    override fun cleanup() {
        context!!.unregisterReceiver(discoverReceiver)
    }
}