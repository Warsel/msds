package com.msds.service.bluetooth.request

import android.content.Context
import com.msds.service.bluetooth.IBluetoothEventListener

interface IBluetoothRequest {
    fun setContext(context: Context)
    fun setEventListener(bluetoothEventListener: IBluetoothEventListener)
    fun cleanup()
}