package com.msds.service.bluetooth.request

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.*
import android.content.Context.BIND_AUTO_CREATE
import android.os.IBinder
import android.util.Log
import com.msds.service.bluetooth.BluetoothLEService
import com.msds.service.bluetooth.IBluetoothDataReceiveEventListener
import com.msds.service.bluetooth.IBluetoothEventListener

class ConnectionRequest : IBluetoothRequest {
    private var context: Context? = null
    private var eventListener: IBluetoothEventListener? = null
    private val bluetoothAdapter: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

    private var currentDevice: BluetoothDevice? = null

    fun connect(device: BluetoothDevice) {
        bluetoothAdapter.cancelDiscovery()
        eventListener!!.onConnecting()

        currentDevice = device
        bluetoothLEService?.connect(currentDevice!!)
    }

    fun sendCommand(command: String) {
        bluetoothLEService?.sendCommand("$command\r\n")
    }

    fun setBluetoothDataReceiveEventListener(eventListener: IBluetoothDataReceiveEventListener) {
        bluetoothLEService?.setEventListener(eventListener)
    }

    private val gattUpdateReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.action) {
                BluetoothLEService.ACTION_GATT_CONNECTED -> {
                    Log.i(null, "ACTION_GATT_CONNECTED")
                }
                BluetoothLEService.ACTION_GATT_DISCONNECTED -> {
                    Log.i(null, "ACTION_GATT_DISCONNECTED")
                    eventListener!!.onDisconnected()
                    bluetoothLEService!!.cleanup()
                    currentDevice = null
                }
                BluetoothLEService.ACTION_GATT_SERVICES_DISCOVERED -> {
                    Log.i(null, "ACTION_GATT_SERVICES_DISCOVERED")
                    eventListener!!.onConnected(bluetoothLEService?.findNecessaryGattServices())
                }
                BluetoothLEService.ACTION_DATA_AVAILABLE -> {
                    Log.i(null, "ACTION_DATA_AVAILABLE")
                }
            }
        }
    }

    private fun registerReceiver() {
        context!!.registerReceiver(gattUpdateReceiver, gattUpdateIntentFilter())
    }

    private fun registerService() {
        if (bluetoothLEService == null) {
            val gattServiceIntent = Intent(context, BluetoothLEService::class.java)
            context!!.bindService(gattServiceIntent, serviceConnection, BIND_AUTO_CREATE)
        }
    }

    override fun setContext(context: Context) {
        this.context = context
        registerReceiver()
        registerService()
    }

    override fun setEventListener(bluetoothEventListener: IBluetoothEventListener) {
        this.eventListener = bluetoothEventListener
    }

    override fun cleanup() {
        currentDevice = null
        bluetoothLEService!!.cleanup()
        //context!!.unregisterReceiver(gattUpdateReceiver)
        //context!!.unbindService(serviceConnection)
    }

    companion object {
        var bluetoothLEService: BluetoothLEService? = null

        private fun gattUpdateIntentFilter(): IntentFilter {
            val intentFilter = IntentFilter()
            intentFilter.addAction(BluetoothLEService.ACTION_GATT_CONNECTED)
            intentFilter.addAction(BluetoothLEService.ACTION_GATT_DISCONNECTED)
            intentFilter.addAction(BluetoothLEService.ACTION_GATT_SERVICES_DISCOVERED)
            intentFilter.addAction(BluetoothLEService.ACTION_DATA_AVAILABLE)
            return intentFilter
        }

        private val serviceConnection: ServiceConnection = object : ServiceConnection {
            override fun onServiceConnected(componentName: ComponentName, service: IBinder) {
                bluetoothLEService = (service as BluetoothLEService.LocalBinder).getService()
            }

            override fun onServiceDisconnected(componentName: ComponentName) {
                bluetoothLEService?.cleanup()
                bluetoothLEService = null
            }
        }
    }
}