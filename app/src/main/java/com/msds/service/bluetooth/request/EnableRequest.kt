package com.msds.service.bluetooth.request

import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.util.Log
import com.msds.service.bluetooth.IBluetoothEventListener

class EnableRequest : IBluetoothRequest {
    private var context: Context? = null
    private var eventListener: IBluetoothEventListener? = null
    private val bluetoothAdapter: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

    private var requestEnable = false

    fun isBluetoothEnabled(): Boolean {
        return bluetoothAdapter.isEnabled
    }

    fun enableBluetooth() {
        if (!bluetoothAdapter.isEnabled) {
            requestEnable = true
            bluetoothAdapter.enable()
        }
        else {
            eventListener!!.onEnable()
        }
    }

    fun disableBluetooth() {
        bluetoothAdapter.disable()
    }

    private val enableReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (!requestEnable && BluetoothAdapter.ACTION_STATE_CHANGED != action) {
                return
            }

            requestEnable = false

            if (bluetoothAdapter.isEnabled) {
                eventListener!!.onEnable()
            } else {
                eventListener!!.onDisable()
            }
        }
    }

    private fun registerReceiver() {
        context!!.registerReceiver(enableReceiver, IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED))
    }

    override fun setContext(context: Context) {
        this.context = context
        registerReceiver()
    }

    override fun setEventListener(bluetoothEventListener: IBluetoothEventListener) {
        this.eventListener = bluetoothEventListener
    }

    override fun cleanup() {
        context!!.unregisterReceiver(enableReceiver)
    }
}