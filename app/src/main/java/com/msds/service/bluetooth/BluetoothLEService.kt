package com.msds.service.bluetooth

import android.app.Service
import android.bluetooth.*
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import java.util.*

class BluetoothLEService : Service() {
    private var bluetoothDataReceiveEventListener: IBluetoothDataReceiveEventListener? = null

    private var bluetoothGatt: BluetoothGatt? = null

    private var connectionState = STATE_DISCONNECTED
    private var bluetoothAddress = ""
    private val binder = LocalBinder()

    private var rxGattCharacteristic: BluetoothGattCharacteristic? = null
    private var txGattCharacteristic: BluetoothGattCharacteristic? = null

    private val gattCallback = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(
            gatt: BluetoothGatt,
            status: Int,
            newState: Int
        ) {
            val intentAction: String
            when (newState) {
                BluetoothProfile.STATE_CONNECTED -> {
                    intentAction = ACTION_GATT_CONNECTED
                    connectionState = STATE_CONNECTED
                    broadcastUpdate(intentAction)
                    bluetoothGatt?.discoverServices()
                }
                BluetoothProfile.STATE_DISCONNECTED -> {
                    intentAction = ACTION_GATT_DISCONNECTED
                    connectionState = STATE_DISCONNECTED
                    broadcastUpdate(intentAction)
                }
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            when (status) {
                BluetoothGatt.GATT_SUCCESS -> {
                    broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED)
                }
            }
        }

        override fun onCharacteristicChanged(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?
        ) {
            characteristic?.apply {
                when (uuid) {
                    UUID_RX_DATA_CHARACTERISTIC -> {
                        bluetoothDataReceiveEventListener?.onDataReceive(value)
                    }
                }
            }
        }
    }

    fun setEventListener(eventListener: IBluetoothDataReceiveEventListener) {
        this.bluetoothDataReceiveEventListener = eventListener
    }

    private fun broadcastUpdate(action: String) {
        val intent = Intent(action)
        sendBroadcast(intent)
    }

    private fun setNotification(characteristic: BluetoothGattCharacteristic) {
        bluetoothGatt?.apply {
            setCharacteristicNotification(characteristic, true)
            val descriptor = characteristic.getDescriptor(UUID_CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR)
            descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
            bluetoothGatt!!.writeDescriptor(descriptor)
        }
    }

    fun connect(device: BluetoothDevice) {
        if (bluetoothGatt != null && device.address == bluetoothAddress) {
            if (bluetoothGatt!!.connect()) {
                connectionState = STATE_CONNECTING
            }
            return
        }

        bluetoothGatt = device.connectGatt(this, false, gattCallback)
        bluetoothAddress = device.address
        connectionState = STATE_CONNECTING
    }

    fun sendCommand(command: String) {
        txGattCharacteristic?.apply {
            setValue(command)
            bluetoothGatt?.apply {
                writeCharacteristic(txGattCharacteristic)
            }
        }
    }

    fun findNecessaryGattServices(): Boolean {
        val gattServices = bluetoothGatt?.services ?: return false

        var uuid: String?
        var serviceName: String?
        var characteristicName: String?

        for (gattService in gattServices) {
            uuid = gattService.uuid.toString()
            serviceName = GattAttributes.lookup(uuid)

            serviceName?.apply {
                for (gattCharacteristic in gattService.characteristics) {
                    uuid = gattCharacteristic.uuid.toString()
                    characteristicName = GattAttributes.lookup(uuid)

                    characteristicName?.apply {
                        when (characteristicName) {
                            GattAttributes.NAME_RX_DATA_CHARACTERISTIC -> {
                                rxGattCharacteristic = gattCharacteristic
                                setNotification(rxGattCharacteristic!!)
                            }
                            GattAttributes.NAME_TX_DATA_CHARACTERISTIC -> {
                                txGattCharacteristic = gattCharacteristic
                            }
                        }
                    }
                }
            }
        }

        if (rxGattCharacteristic == null || txGattCharacteristic == null) {
            return false
        }

        return true
    }

    private fun disconnect() {
        bluetoothGatt?.close()
    }

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    fun cleanup() {
        disconnect()
        bluetoothGatt = null
    }

    inner class LocalBinder : Binder() {
        fun getService(): BluetoothLEService {
            return this@BluetoothLEService
        }
    }

    companion object {
        private const val STATE_DISCONNECTED = 0
        private const val STATE_CONNECTING = 1
        private const val STATE_CONNECTED = 2
        const val ACTION_GATT_CONNECTED = "com.msds.service.bluetooth.ACTION_GATT_CONNECTED"
        const val ACTION_GATT_DISCONNECTED = "com.msds.service.bluetooth.ACTION_GATT_DISCONNECTED"
        const val ACTION_GATT_SERVICES_DISCOVERED = "com.msds.service.bluetooth.ACTION_GATT_SERVICES_DISCOVERED"
        const val ACTION_DATA_AVAILABLE = "com.msds.service.bluetooth.ACTION_DATA_AVAILABLE"
        val UUID_RX_DATA_CHARACTERISTIC: UUID = UUID.fromString(GattAttributes.UUID_RX_DATA_CHARACTERISTIC)
        val UUID_CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR: UUID =  UUID.fromString(GattAttributes.UUID_CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR)
    }
}