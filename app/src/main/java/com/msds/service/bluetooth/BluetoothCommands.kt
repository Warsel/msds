package com.msds.service.bluetooth

object BluetoothCommands {
    const val start = "AdcStart"
    const val stop = "AdcStop"
}