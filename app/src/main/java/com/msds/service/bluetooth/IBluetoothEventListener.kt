package com.msds.service.bluetooth

import android.bluetooth.BluetoothDevice

interface IBluetoothEventListener {
    fun onEnable()
    fun onDisable()
    fun onDiscovering()
    fun onDiscovered()
    fun onDeviceFound(devicesList: ArrayList<BluetoothDevice>)
    fun onConnecting()
    fun onConnected(isSuccess: Boolean?)
    fun onDisconnecting()
    fun onDisconnected()
}