package com.msds.service.bluetooth

import android.bluetooth.BluetoothDevice
import android.content.Context
import android.util.Log
import com.msds.service.bluetooth.request.ConnectionRequest
import com.msds.service.bluetooth.request.DiscoverRequest
import com.msds.service.bluetooth.request.EnableRequest

object BluetoothConnectionService {
    private val enableRequest = EnableRequest()
    private val discoverRequest = DiscoverRequest()
    private val connectionRequest = ConnectionRequest()

    fun setContext(context: Context) {
        this.enableRequest.setContext(context)
        this.discoverRequest.setContext(context)
        this.connectionRequest.setContext(context)
    }

    fun setEventListener(bluetoothEventListener: IBluetoothEventListener) {
        this.enableRequest.setEventListener(bluetoothEventListener)
        this.discoverRequest.setEventListener(bluetoothEventListener)
        this.connectionRequest.setEventListener(bluetoothEventListener)
    }

    fun isBluetoothEnabled(): Boolean {
        return enableRequest.isBluetoothEnabled()
    }

    fun enableBluetoothAdapter() {
        enableRequest.enableBluetooth()
    }

    fun disableBluetoothAdapter() {
        enableRequest.disableBluetooth()
    }

    fun discoverDevices() {
        discoverRequest.discover()
    }

    fun connectDevice(device: BluetoothDevice) {
        connectionRequest.connect(device)
    }

    fun sendCommand(command: String) {
        connectionRequest.sendCommand(command)
    }

    fun setBluetoothDataReceiveEventListener(eventListener: IBluetoothDataReceiveEventListener) {
        this.connectionRequest.setBluetoothDataReceiveEventListener(eventListener)
    }

    fun cleanUp() {
        Log.i(null, "cleanUp")
        // enableRequest.cleanup()
        // discoverRequest.cleanup()
        connectionRequest.cleanup()
    }
}