package com.msds.service.helper

import kotlin.random.Random

object Utils {
    private val charPool : List<Char> = ('a'..'z') + ('0'..'9')

    fun generateUserKey(size: Int = 6): String {
        return (1..size)
            .map { Random.nextInt(0, charPool.size) }
            .map(charPool::get)
            .joinToString("")
    }
}