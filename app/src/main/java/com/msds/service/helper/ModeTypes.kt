package com.msds.service.helper

enum class ModeTypes {
    MANOMETER,
    RIPPLE_ANALYSIS
}