package com.msds.viewmodel

import android.bluetooth.BluetoothDevice
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import com.msds.service.GlobalState
import com.msds.service.analysis.IChannelsValuesUpdateEventListener
import com.msds.service.analysis.IDataAnalysis
import com.msds.service.analysis.ManometerAnalysis
import com.msds.service.analysis.PulseAnalysis
import com.msds.service.bluetooth.BluetoothCommands
import com.msds.service.bluetooth.BluetoothConnectionService
import com.msds.service.bluetooth.IBluetoothDataReceiveEventListener
import com.msds.service.bluetooth.IBluetoothEventListener
import com.msds.service.helper.ModeTypes
import com.msds.service.model.ChannelNumber
import com.msds.service.model.WorkPlace
import com.msds.view.listener.IBluetoothRequestEventListener
import com.msds.view.listener.IUIEventListener
import java.util.*
import kotlin.concurrent.timer

class GraphViewModel : ViewModel(), IBluetoothEventListener, IBluetoothDataReceiveEventListener,
    IChannelsValuesUpdateEventListener {
    var firstSeries: LineGraphSeries<DataPoint> = LineGraphSeries()
    var secondSeries: LineGraphSeries<DataPoint> = LineGraphSeries()

    private val _firstChannelMinVacuumValue = MutableLiveData<String>()
    private val _firstChannelMaxVacuumValue = MutableLiveData<String>()
    private val _firstChannelCurrentVacuumValue = MutableLiveData<String>()
    private val _firstChannelPulseCountValue = MutableLiveData<String>()
    private val _firstChannelEPhaseValue = MutableLiveData<String>()
    private val _firstChannelFPhaseValue = MutableLiveData<String>()
    private val _secondChannelMinVacuumValue = MutableLiveData<String>()
    private val _secondChannelMaxVacuumValue = MutableLiveData<String>()
    private val _secondChannelCurrentVacuumValue = MutableLiveData<String>()
    private val _secondChannelPulseCountValue = MutableLiveData<String>()
    private val _secondChannelEPhaseValue = MutableLiveData<String>()
    private val _secondChannelFPhaseValue = MutableLiveData<String>()

    private val _isManometerMode = MutableLiveData<Int>()
    private val _isRippleAnalysisMode = MutableLiveData<Int>()
    private var _modeType: ModeTypes? = null

    private val _packageCount = MutableLiveData<String>()
    private var _packageCountTemp = 0

    private var _isMeasuringStarted = false
    private val _startOrStopButtonText = MutableLiveData<String>()

    var selectedOrganizationIndex: Int = 0
    var selectedFarmIndex: Int = 0

    private val _selectedWorkplace = MutableLiveData<String>()
    private val _isPreviewButtonEnabled = MutableLiveData<Boolean>()

    private var _bluetoothRequestEventListener: IBluetoothRequestEventListener? = null
    private var _uiEventListener: IUIEventListener? = null

    private var _packageCountUpdateTimer: Timer? = null
    private var dataAnalysis: IDataAnalysis? = null

    init {
        _isManometerMode.value = View.GONE
        _isRippleAnalysisMode.value = View.GONE
        _startOrStopButtonText.value = "start"
        _selectedWorkplace.value = "1"

    }

    fun getFirstChannelMinVacuumValue(): LiveData<String> {
        return _firstChannelMinVacuumValue
    }

    fun getFirstChannelMaxVacuumValue(): LiveData<String> {
        return _firstChannelMaxVacuumValue
    }

    fun getFirstChannelCurrentVacuumValue(): LiveData<String> {
        return _firstChannelCurrentVacuumValue
    }

    fun getFirstChannelNPhaseValue(): LiveData<String> {
        return _firstChannelPulseCountValue
    }

    fun getFirstChannelEPhaseValue(): LiveData<String> {
        return _firstChannelEPhaseValue
    }

    fun getFirstChannelFPhaseValue(): LiveData<String> {
        return _firstChannelFPhaseValue
    }

    fun getSecondChannelMinVacuumValue(): LiveData<String> {
        return _secondChannelMinVacuumValue
    }

    fun getSecondChannelMaxVacuumValue(): LiveData<String> {
        return _secondChannelMaxVacuumValue
    }

    fun getSecondChannelCurrentVacuumValue(): LiveData<String> {
        return _secondChannelCurrentVacuumValue
    }

    fun getSecondChannelNPhaseValue(): LiveData<String> {
        return _secondChannelPulseCountValue
    }

    fun getSecondChannelEPhaseValue(): LiveData<String> {
        return _secondChannelEPhaseValue
    }

    fun getSecondChannelFPhaseValue(): LiveData<String> {
        return _secondChannelFPhaseValue
    }

    fun startOrStopMeasuring() {
        if (!_isMeasuringStarted) {
            dataAnalysis!!.startCurrentValueUpdateTimer()
            startPackageCountUpdateTimer()
            BluetoothConnectionService.sendCommand(BluetoothCommands.start)
            _isMeasuringStarted = true
            _startOrStopButtonText.value = "stop"
        } else {
            dataAnalysis!!.stopCurrentValueUpdateTimer()
            stopPackageCountUpdateTimer()
            BluetoothConnectionService.sendCommand(BluetoothCommands.stop)
            _isMeasuringStarted = false
            _startOrStopButtonText.value = "start"
        }

    }

    fun stopMeasuring() {
        dataAnalysis!!.stopCurrentValueUpdateTimer()
        stopPackageCountUpdateTimer()
        BluetoothConnectionService.sendCommand(BluetoothCommands.stop)
        _isMeasuringStarted = false
        _startOrStopButtonText.value = "start"
    }

    fun getStartOrStopButtonText(): LiveData<String> {
        return _startOrStopButtonText
    }

    fun getSelectedWorkplace(): LiveData<String> {
        return _selectedWorkplace
    }

    fun selectNextWorkplace() {
        val selectedWorkspace = _selectedWorkplace.value!!.toInt() - 1 + 1
        if (GlobalState.user!!.organizations[selectedOrganizationIndex].farms[selectedFarmIndex].workplaces.size == selectedWorkspace) {
            GlobalState.user!!.organizations[selectedOrganizationIndex].farms[selectedFarmIndex].workplaces.add(WorkPlace())
        }

        _isPreviewButtonEnabled.value = true
        _selectedWorkplace.value = (selectedWorkspace + 1).toString()
    }

    fun selectPreviewWorkplace() {
        val selectedWorkspace = _selectedWorkplace.value!!.toInt() - 1 - 1
        if (selectedWorkspace == -1) {
            return
        }

        if (selectedWorkspace == 0) {
            _isPreviewButtonEnabled.value = false
        }
        _selectedWorkplace.value = (selectedWorkspace + 1).toString()
    }

    fun getIsPreviewButtonEnabled(): LiveData<Boolean> {
        return _isPreviewButtonEnabled
    }

    fun getPackageCount(): LiveData<String> {
        return _packageCount
    }

    fun saveMeasuring() {
        dataAnalysis!!.save(_selectedWorkplace.value!!.toInt() - 1)
    }

    fun getIsManometerMode(): LiveData<Int> {
        return _isManometerMode
    }

    fun getIsRippleAnalysisMode(): LiveData<Int> {
        return _isRippleAnalysisMode
    }

    private fun setPackageCountValues() {
        _packageCount.value = _packageCountTemp.toString()
        _packageCountTemp = 0
    }

    private fun onPackageReceive() {
        _packageCountTemp += 1
    }

    private fun startPackageCountUpdateTimer() {
        _packageCountUpdateTimer = timer(
            null,
            false,
            0,
            1000
        ) {
            _uiEventListener?.runAction { setPackageCountValues() }

            if (_modeType == ModeTypes.RIPPLE_ANALYSIS) {
                _uiEventListener?.runAction {  }
            }
        }
    }

    private fun stopPackageCountUpdateTimer() {
        _packageCountUpdateTimer?.cancel()
    }

    private fun setChannelsCurrentValues(value: String, channelNumber: ChannelNumber) {
        when (channelNumber) {
            ChannelNumber.FIRST -> {
                _firstChannelCurrentVacuumValue.value = value
            }
            ChannelNumber.SECOND -> {
                _secondChannelCurrentVacuumValue.value = value
            }
        }
    }

    override fun onChannelMaxVacuumValueUpdate(value: String, channelNumber: ChannelNumber) {
        when (channelNumber) {
            ChannelNumber.FIRST -> {
                _firstChannelMaxVacuumValue.value = value
            }
            ChannelNumber.SECOND -> {
                _secondChannelMaxVacuumValue.value = value
            }
        }
    }

    override fun onChannelMinVacuumValueUpdate(value: String, channelNumber: ChannelNumber) {
        when (channelNumber) {
            ChannelNumber.FIRST -> {
                _firstChannelMinVacuumValue.value = value
            }
            ChannelNumber.SECOND -> {
                _secondChannelMinVacuumValue.value = value
            }
        }
    }

    override fun onChannelCurrentVacuumValueUpdate(value: String, channelNumber: ChannelNumber) {
        _uiEventListener!!.runAction {
            setChannelsCurrentValues(value, channelNumber)
        }
    }

    override fun onChannelPropertiesUpdate(
        maxVacuum: String,
        minVacuum: String,
        pulseCount: String,
        ePhase: String,
        fPhase: String,
        channelNumber: ChannelNumber
    ) {
        when (channelNumber) {
            ChannelNumber.FIRST -> {
                _firstChannelMaxVacuumValue.value = maxVacuum
                _firstChannelMinVacuumValue.value = minVacuum
                _firstChannelPulseCountValue.value = pulseCount
                _firstChannelEPhaseValue.value = ePhase
                _firstChannelFPhaseValue.value = fPhase
            }
            ChannelNumber.SECOND -> {
                _secondChannelMaxVacuumValue.value = maxVacuum
                _secondChannelMinVacuumValue.value = minVacuum
                _secondChannelPulseCountValue.value = pulseCount
                _secondChannelEPhaseValue.value = ePhase
                _secondChannelFPhaseValue.value = fPhase
            }
        }
    }

    override fun onEnable() { }

    override fun onDisable() { }

    override fun onDiscovering() { }

    override fun onDiscovered() { }

    override fun onDeviceFound(devicesList: ArrayList<BluetoothDevice>) { }

    override fun onConnecting() { }

    override fun onConnected(isSuccess: Boolean?) { }

    override fun onDisconnecting() { }

    override fun onDisconnected() {
        _bluetoothRequestEventListener!!.onDeviceDisconnected()
    }

    override fun onDataReceive(data: ByteArray) {
        if (data.size != 20) {
            return
        }

        onPackageReceive()
        _uiEventListener!!.runAction {
            dataAnalysis!!.addPoints(data)
        }
    }

    fun setBluetoothEnableRequestEventListener(eventListener: IBluetoothRequestEventListener) {
        _bluetoothRequestEventListener = eventListener
    }

    fun setUIEventListener(eventListener: IUIEventListener) {
        _uiEventListener = eventListener
    }

    fun setMode(mode: String, selectedOrganization: Int, selectedFarm: Int) {
        when (mode) {
            ModeTypes.MANOMETER.toString() -> {
                _isManometerMode.value = View.VISIBLE
                _modeType = ModeTypes.MANOMETER
                dataAnalysis = ManometerAnalysis(firstSeries, secondSeries, this)
            }
            ModeTypes.RIPPLE_ANALYSIS.toString() -> {
                _isRippleAnalysisMode.value = View.VISIBLE
                _modeType = ModeTypes.RIPPLE_ANALYSIS
                dataAnalysis = PulseAnalysis(firstSeries, secondSeries, selectedOrganization, selectedFarm, this)
            }
        }
    }
}