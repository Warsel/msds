package com.msds.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.msds.service.GlobalState
import com.msds.service.model.Organization
import com.msds.view.listener.IStorageEventListener

class StorageViewModel : ViewModel() {
    private val _organizations = MutableLiveData<List<Organization>>()

    private var _storageEventListener: IStorageEventListener? = null

    init {
        _organizations.value = GlobalState.user!!.organizations
    }

    fun getOrganizations(): LiveData<List<Organization>> {
        return _organizations
    }

    fun onAddNewOrganization() {
        _storageEventListener!!.onAddNewOrganization()
    }

    fun setEventListener(eventListener: IStorageEventListener) {
        _storageEventListener = eventListener
    }
}