package com.msds.viewmodel

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.msds.service.GlobalState
import com.msds.service.helper.Utils
import com.msds.service.model.Farm
import com.msds.service.model.Organization
import com.msds.service.model.User
import com.msds.service.model.WorkPlace
import com.msds.service.repository.UserRepository

class AuthViewModel : ViewModel() {
    val userKey = MutableLiveData<String>()
    val isDataLoading = MutableLiveData<Boolean>()
    val isProgressBarVisible = MutableLiveData<Int>()

    init {
        isDataLoading.value = false
        isProgressBarVisible.value = View.GONE
    }

    private val _generatedUserKey = MutableLiveData<String>()
    val generatedUserKey: LiveData<String>
        get() = _generatedUserKey

    fun generateUserKey() {
        if (_generatedUserKey.value.isNullOrEmpty()) {
            _generatedUserKey.value = Utils.generateUserKey()
        }
    }

    fun enableLoading() {
        isDataLoading.value = true
        isProgressBarVisible.value = View.VISIBLE
    }

    fun disableLoading() {
        isDataLoading.value = false
        isProgressBarVisible.value = View.GONE
    }

    suspend fun signUp() {
        if (!_generatedUserKey.value.isNullOrEmpty()) {
            val user = User()
//            val workPlace = WorkPlace("Workplace #1")
//            val farm = Farm("Farm #1")
//            farm.workplaces.add(workPlace)
//            val organization = Organization("Organization #1")
//            organization.farms.add(farm)
//
//            user.organizations.add(organization)
            GlobalState.userId = _generatedUserKey.value!!
            GlobalState.user = user
            UserRepository.addUser(_generatedUserKey.value!!, user)
        }
    }

    suspend fun signIn(): Boolean {
        if (!userKey.value.isNullOrEmpty()) {
            return UserRepository.isUserExist(userKey.value!!)
        }

        return false
    }
}