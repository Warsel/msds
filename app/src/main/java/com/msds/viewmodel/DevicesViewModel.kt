package com.msds.viewmodel

import android.bluetooth.BluetoothDevice
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.msds.service.bluetooth.BluetoothConnectionService
import com.msds.service.bluetooth.IBluetoothEventListener
import com.msds.view.listener.IBluetoothRequestEventListener

class DevicesViewModel : ViewModel(), IBluetoothEventListener {
    private val _isDataLoading = MutableLiveData<Boolean>()
    private val _isProgressBarVisible = MutableLiveData<Int>()

    private val _discoveredDevices = MutableLiveData<ArrayList<BluetoothDevice>>()
    private var _bluetoothRequestEventListener: IBluetoothRequestEventListener? = null

    init {
        _isDataLoading.value = false
        _isProgressBarVisible.value = View.GONE
        _discoveredDevices.value = ArrayList()
    }

    fun getDataLoadingStatus(): LiveData<Boolean> {
        return _isDataLoading
    }

    fun getProgressBarVisibilityStatus(): LiveData<Int> {
        return _isProgressBarVisible
    }

    private fun isBluetoothEnabled(): Boolean {
        return BluetoothConnectionService.isBluetoothEnabled()
    }

    fun onStartDiscover() {
        BluetoothConnectionService.cleanUp()

        enableLoading()

        if (!isBluetoothEnabled()) {
            _bluetoothRequestEventListener!!.onBluetoothEnableRequest()
        } else {
            startDiscover()
        }
    }

    fun startDiscover() {
        BluetoothConnectionService.discoverDevices()
    }

    fun getDiscoveredDevices(): LiveData<ArrayList<BluetoothDevice>> {
        return _discoveredDevices
    }

    private fun enableLoading() {
        _isDataLoading.value = true
        _isProgressBarVisible.value = View.VISIBLE
    }

    fun disableLoading() {
        _isDataLoading.value = false
        _isProgressBarVisible.value = View.GONE
    }

    override fun onEnable() {
        Log.d(null, "onEnable")
    }

    override fun onDisable() {
        Log.d(null, "onDisable")
        _discoveredDevices.value = ArrayList()
    }

    override fun onDiscovering() {
        Log.d(null, "onDiscovering")
        _discoveredDevices.value = ArrayList()
    }

    override fun onDiscovered() {
        Log.d(null, "onDiscovered")
        disableLoading()
    }

    override fun onDeviceFound(devicesList: ArrayList<BluetoothDevice>) {
        _discoveredDevices.value = devicesList
    }

    override fun onConnecting() {
        Log.d(null, "onConnecting")
    }

    override fun onConnected(isSuccess: Boolean?) {
        this._bluetoothRequestEventListener!!.onDeviceConnected(isSuccess)

        if (isSuccess == true) {
            _discoveredDevices.value = ArrayList()
        }
    }

    override fun onDisconnecting() {
        Log.i(null, "onDisconnecting")
    }

    override fun onDisconnected() {
        this._bluetoothRequestEventListener!!.onDeviceDisconnected()
    }

    fun setBluetoothEnableRequestEventListener(eventListener: IBluetoothRequestEventListener) {
        _bluetoothRequestEventListener = eventListener
    }
}