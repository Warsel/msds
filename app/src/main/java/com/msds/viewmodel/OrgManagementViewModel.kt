package com.msds.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.msds.service.GlobalState
import com.msds.service.model.Farm
import com.msds.service.model.Organization
import com.msds.service.repository.UserRepository
import com.msds.view.listener.IUIEventListener
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class OrgManagementViewModel : ViewModel() {
    val organizationName = MutableLiveData<String>()
    val farmName = MutableLiveData<String>()
    private val _farms = MutableLiveData<MutableList<Farm>>()

    private var _uiEventListener: IUIEventListener? = null

    init {
        _farms.value = mutableListOf()
    }

    fun getFarms(): LiveData<MutableList<Farm>> {
        return  _farms
    }

    fun addNewFarm() {
        val farmList: MutableList<Farm> = mutableListOf()
        farmList.addAll(_farms.value!!)
        farmList.add(Farm(farmName.value!!))

        _farms.value = farmList
        farmName.value = ""
    }

    fun save() {
        val organization = Organization(organizationName.value!!, _farms.value!!)
        GlobalState.user!!.organizations.add(organization)

        GlobalScope.launch {
            UserRepository.updateUser(GlobalState.userId!!, GlobalState.user!!)
        }

        _uiEventListener!!.finishActivity()
    }

    fun setUIEventListener(eventListener: IUIEventListener) {
        _uiEventListener = eventListener
    }
}