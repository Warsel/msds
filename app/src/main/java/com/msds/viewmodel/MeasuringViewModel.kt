package com.msds.viewmodel

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.msds.service.GlobalState

class MeasuringViewModel : ViewModel() {
    private val _isMeasuringEnable = MutableLiveData<Int>()
    private val _isMeasuringDisable = MutableLiveData<Int>()
    private val _selectedOrganization = MutableLiveData<String>()
    private val _selectedFarm = MutableLiveData<String>()
    private var _organizationsAndFarms = HashMap<String, ArrayList<String>>()

    init {
        _isMeasuringEnable.value = View.VISIBLE
        _isMeasuringDisable.value = View.GONE
        _organizationsAndFarms = getOrganizationsAndFarms()
    }

    fun getIsMeasuringEnable(): LiveData<Int> {
        return _isMeasuringEnable
    }

    fun getIsMeasuringDisable(): LiveData<Int> {
        return _isMeasuringDisable
    }

    fun onSelectOrganization(organizationName: String) {
        _selectedOrganization.value = organizationName
    }

    fun onSelectFarm(farm: String) {
        _selectedFarm.value = farm
    }

    fun getSelectedOrganization(): LiveData<String> {
        return _selectedOrganization
    }

    fun getSelectedFarm(): LiveData<String> {
        return _selectedFarm
    }

    fun getOrganizationsNames(): ArrayList<String> {
        _organizationsAndFarms = getOrganizationsAndFarms()
        return ArrayList(_organizationsAndFarms.keys)
    }

    fun getFarmsNames(organizationName: String): ArrayList<String> {
        return _organizationsAndFarms[organizationName]!!
    }

    private fun getOrganizationsAndFarms(): HashMap<String, ArrayList<String>> {
        if (GlobalState.user!!.organizations.size == 0) {
            _isMeasuringEnable.value = View.GONE
            _isMeasuringDisable.value = View.VISIBLE
        }

        val organizationsAndFarmsNames = HashMap<String, ArrayList<String>>()
        for (organization in GlobalState.user!!.organizations) {
            val farmsNames = arrayListOf<String>()
            for (farm in organization.farms) {
                farmsNames.add(farm.name)
            }

            organizationsAndFarmsNames[organization.name] = farmsNames
        }

        return organizationsAndFarmsNames
    }

    fun refreshOrganizationAndFarms() {
        if (GlobalState.user!!.organizations.size == 0) {
            _isMeasuringEnable.value = View.GONE
            _isMeasuringDisable.value = View.VISIBLE
        } else {
            _isMeasuringEnable.value = View.VISIBLE
            _isMeasuringDisable.value = View.GONE
        }

        _organizationsAndFarms = getOrganizationsAndFarms()
    }
}