package com.msds.view.adapter

import android.annotation.SuppressLint
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.lifecycle.LiveData
import com.msds.R
import com.msds.service.bluetooth.BluetoothConnectionService
import kotlinx.android.synthetic.main.list_item_bluetooth_device.view.*

class BluetoothDeviceAdapter(
    context: Context,
    private val bluetoothDevicesList: LiveData<ArrayList<BluetoothDevice>>
) : BaseAdapter() {
    private val inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView = inflater.inflate(R.layout.list_item_bluetooth_device, parent, false)
        val bluetoothDevice = bluetoothDevicesList.value!![position]

        if (!bluetoothDevice.name.isNullOrEmpty()) {
            rowView.bluetooth_device_name_text_view.text = bluetoothDevice.name
        } else {
            rowView.bluetooth_device_name_text_view.text = bluetoothDevice.address
        }

        rowView.bluetooth_device.setOnClickListener {
            rowView.connection_progress_bar.visibility = View.VISIBLE
            BluetoothConnectionService.connectDevice(bluetoothDevice)
        }

        return rowView
    }

    override fun getItem(position: Int): Any {
        return bluetoothDevicesList.value!![position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return bluetoothDevicesList.value!!.size
    }
}