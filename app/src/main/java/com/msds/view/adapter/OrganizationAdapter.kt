package com.msds.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import com.msds.R
import com.msds.service.model.Organization
import kotlinx.android.synthetic.main.list_item_farm.view.*
import kotlinx.android.synthetic.main.list_item_organization.view.*

class OrganizationAdapter(
    context: Context,
    private val organizations: List<Organization>
) : BaseExpandableListAdapter() {
    private val inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    init {

    }

    override fun getGroup(groupPosition: Int): Any {
        return organizations[groupPosition]
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

    override fun hasStableIds(): Boolean {
        return true
    }

    override fun getGroupView(
        groupPosition: Int,
        isExpanded: Boolean,
        convertView: View?,
        parent: ViewGroup?
    ): View {
        val groupView = inflater.inflate(R.layout.list_item_organization, parent, false)
        groupView.organization_name_text_view.text = organizations[groupPosition].name
        return groupView
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return organizations[groupPosition].farms.size
    }

    override fun getChild(groupPosition: Int, childPosition: Int): Any {
        return organizations[groupPosition].farms[childPosition]
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildView(
        groupPosition: Int,
        childPosition: Int,
        isLastChild: Boolean,
        convertView: View?,
        parent: ViewGroup?
    ): View {
        val childView = inflater.inflate(R.layout.list_item_farm, parent, false)
        childView.farm_name_text_view.text = organizations[groupPosition].farms[childPosition].name
        return childView
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return (groupPosition * 100 + childPosition).toLong()
    }

    override fun getGroupCount(): Int {
        return organizations.size
    }
}