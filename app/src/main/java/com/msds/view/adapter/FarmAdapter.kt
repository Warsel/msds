package com.msds.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.lifecycle.LiveData
import com.msds.R
import com.msds.service.model.Farm
import kotlinx.android.synthetic.main.list_item_farm.view.*

class FarmAdapter(
    context: Context,
    private val farms: LiveData<MutableList<Farm>>
) : BaseAdapter() {
    private val inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView = inflater.inflate(R.layout.list_item_farm, parent, false)
        rowView.farm_name_text_view.text = farms.value!![position].name
        return rowView
    }

    override fun getItem(position: Int): Any {
        return farms.value!![position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return farms.value!!.size
    }
}