package com.msds.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.core.content.ContextCompat
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import com.jjoe64.graphview.series.PointsGraphSeries
import com.msds.R
import com.msds.service.model.PulseAnalysisProperties
import kotlinx.android.synthetic.main.list_item_graph.view.*
import kotlin.math.roundToInt

class GraphAdapter(
    private val context: Context,
    private val pulseAnalysisPropertiesList: List<PulseAnalysisProperties>
    ) : BaseAdapter() {
        private val inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    private var series: LineGraphSeries<DataPoint>? = null
    private var indexedSeries: PointsGraphSeries<DataPoint>? = null

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView = convertView ?: inflater.inflate(R.layout.list_item_graph, parent, false)
        val pulseAnalysisProperties = pulseAnalysisPropertiesList[position]

        rowView.pulse_count_text_view.text = "N: " + pulseAnalysisProperties.pulseCount.roundToInt().toString()
        rowView.a_phase_text_view.text = "A: " + pulseAnalysisProperties.aPhase.roundToInt().toString()
        rowView.b_phase_text_view.text = "B: " + pulseAnalysisProperties.bPhase.roundToInt().toString()
        rowView.c_phase_text_view.text = "C: " + pulseAnalysisProperties.cPhase.roundToInt().toString()
        rowView.d_phase_text_view.text = "D: " + pulseAnalysisProperties.dPhase.roundToInt().toString()
        rowView.e_phase_text_view.text = "E: " + pulseAnalysisProperties.ePhase.roundToInt().toString()
        rowView.f_phase_text_view.text = "F: " + pulseAnalysisProperties.fPhase.roundToInt().toString()
        rowView.min_vacuum_value_text_view.text = "min: " + pulseAnalysisProperties.minVacuum.roundToInt().toString()
        rowView.max_vacuum_value_text_view.text ="max: " +  pulseAnalysisProperties.maxVacuum.roundToInt().toString()

        if (series != null) {
            rowView.graph_view.removeSeries(series)
            series = null
        }

        series = LineGraphSeries<DataPoint>()

        if (indexedSeries != null) {
            rowView.graph_view.removeSeries(indexedSeries)
            indexedSeries = null
        }

        indexedSeries = PointsGraphSeries<DataPoint>()

        indexedSeries!!.color = Color.GREEN
        indexedSeries!!.shape = PointsGraphSeries.Shape.TRIANGLE

        var xAxis = 0.0
        for (point in pulseAnalysisProperties.points) {
            series!!.appendData(DataPoint(xAxis, point), true, 1250)

            if (xAxis.toInt() == pulseAnalysisProperties.aPhaseIndex ||
                xAxis.toInt() == pulseAnalysisProperties.bPhaseIndex ||
                xAxis.toInt() == pulseAnalysisProperties.cPhaseIndex ||
                xAxis.toInt() == pulseAnalysisProperties.dPhaseIndex) {
                indexedSeries!!.appendData(DataPoint(xAxis, point), true, 1250)
            }
            xAxis += 1
        }

        if (position == 0) {
            series!!.color = ContextCompat.getColor(context, R.color.colorFirstChannel)
        } else {
            series!!.color = ContextCompat.getColor(context, R.color.colorSecondChannel)
        }

        rowView.graph_view.viewport.isXAxisBoundsManual = true
        rowView.graph_view.viewport.setMinX(0.0)
        rowView.graph_view.viewport.setMaxX(pulseAnalysisProperties.points.size.toDouble())

        rowView.graph_view.viewport.isYAxisBoundsManual = true
        rowView.graph_view.viewport.setMinY(0.0)
        rowView.graph_view.viewport.setMaxY(pulseAnalysisProperties.maxVacuum + pulseAnalysisProperties.maxVacuum * 0.05)

        rowView.graph_view.addSeries(series)
        rowView.graph_view.addSeries(indexedSeries)

        return rowView
    }

    override fun getItem(position: Int): Any {
        return pulseAnalysisPropertiesList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return pulseAnalysisPropertiesList.size
    }

    private fun getSeriesFromPoints(points: ArrayList<Double>): LineGraphSeries<DataPoint> {
        var xAxis = 0.0
        val series = LineGraphSeries<DataPoint>()

        for (point in points) {
            series.appendData(DataPoint(xAxis, point), true, 1250)
            xAxis += 1
        }

        return series
    }
}