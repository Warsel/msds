package com.msds.view.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.msds.service.GlobalState
import com.msds.view.ui.storage.GraphPageFragment

class GraphPagerAdapter(
    fragmentManager: FragmentManager,
    private val selectedOrganizationIndex: Int,
    private val selectedFarmIndex: Int
) : FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): Fragment {
        return GraphPageFragment.newInstance(position, selectedOrganizationIndex, selectedFarmIndex)
    }

    override fun getCount(): Int {
        return GlobalState.user!!.organizations[selectedOrganizationIndex].farms[selectedFarmIndex].workplaces.size
    }
}