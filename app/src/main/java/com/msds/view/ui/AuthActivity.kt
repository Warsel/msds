package com.msds.view.ui

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.msds.R

class AuthActivity : AppCompatActivity() {
    private lateinit var clipboardManager: ClipboardManager
    private lateinit var clipData: ClipData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        clipboardManager = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
    }

    fun startMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun saveUserKeyToPreference(userKey: String) {
        val prefs = getSharedPreferences("userInfo", Context.MODE_PRIVATE)
        val editor = prefs.edit()

        editor.putString("userKey", userKey)
        editor.apply()
    }

    fun copyTextToClipboard(text: String) {
        clipData = ClipData.newPlainText("text", text)
        clipboardManager.setPrimaryClip(clipData)

        Toast.makeText(this, "Индификатор скопирован", Toast.LENGTH_SHORT).show()
    }
}