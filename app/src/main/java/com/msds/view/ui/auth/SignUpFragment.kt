package com.msds.view.ui.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.msds.R
import com.msds.databinding.FragmentSignUpBinding
import com.msds.service.GlobalState
import com.msds.view.ui.AuthActivity
import com.msds.viewmodel.AuthViewModel

class SignUpFragment : Fragment() {

    private lateinit var authViewModel: AuthViewModel
    private lateinit var binding: FragmentSignUpBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        authViewModel = ViewModelProvider(activity!!).get(AuthViewModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_up, container, false)

        binding.apply {
            lifecycleOwner = this@SignUpFragment
            viewmodel = authViewModel
        }

        binding.continueButton.setOnClickListener {
            (activity as AuthActivity).apply {
                GlobalState.userId = authViewModel.generatedUserKey.value!!
                saveUserKeyToPreference(authViewModel.generatedUserKey.value!!)
                startMainActivity()
            }
        }

        binding.copyGeneratedUserKeyButton.setOnClickListener {
            (activity as AuthActivity).copyTextToClipboard(authViewModel.generatedUserKey.value!!)
        }

        return binding.root
    }
}