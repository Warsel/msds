package com.msds.view.ui.storage

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.msds.R
import com.msds.service.GlobalState
import com.msds.view.adapter.GraphAdapter
import kotlinx.android.synthetic.main.fragment_graph_page.view.*

class GraphPageFragment : Fragment() {
    private var workplaceNumber: Int = 0
    private var selectedOrganizationIndex: Int = -1
    private var selectedFarmIndex: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        workplaceNumber = arguments!!.getInt("workplaceNumber")
        selectedOrganizationIndex = arguments!!.getInt("selectedOrganizationIndex")
        selectedFarmIndex = arguments!!.getInt("selectedFarmIndex")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_graph_page, container, false)
        root.workplace_number_text_view.text = (workplaceNumber + 1).toString()

        val adapter = GraphAdapter(
            context!!,
            arrayListOf(
                GlobalState.user!!.organizations[selectedOrganizationIndex].farms[selectedFarmIndex].workplaces[workplaceNumber].firstChannelProperties,
                GlobalState.user!!.organizations[selectedOrganizationIndex].farms[selectedFarmIndex].workplaces[workplaceNumber].secondChannelProperties
            )
        )

        root.graph_properties_list_view.adapter = adapter

        return root
    }

    companion object {
        fun newInstance(workplaceNumber: Int, selectedOrganizationIndex: Int, selectedFarmIndex: Int): GraphPageFragment {
            val bundle = Bundle()
            bundle.putInt("workplaceNumber", workplaceNumber)
            bundle.putInt("selectedOrganizationIndex", selectedOrganizationIndex)
            bundle.putInt("selectedFarmIndex", selectedFarmIndex)

            val fragment = GraphPageFragment()
            fragment.arguments = bundle

            return fragment
        }
    }
}