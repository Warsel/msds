package com.msds.view.ui

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.msds.R
import com.msds.service.GlobalState
import com.msds.service.helper.ActivityTypes
import com.msds.service.repository.UserRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class LaunchActivity : AppCompatActivity() {
    private val REQUEST_ACCESS_FINE_LOCATION = 100
    private val permissions =  arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)

    override fun onStart() {
        super.onStart()

        if (ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            val builder = AlertDialog.Builder(this)
            builder.setMessage(getString(R.string.about_permission))
                .setPositiveButton(getString(R.string.yes)) { _, _ ->
                    ActivityCompat.requestPermissions(this, permissions, REQUEST_ACCESS_FINE_LOCATION)
                }
                .setNegativeButton(getString(R.string.no)) { _, _ ->
                    this.finish()
                }
                .create()
                .show()
        } else {
            startActivity()
        }
    }

    private fun startActivity() {
        val prefs = getSharedPreferences("userInfo", Context.MODE_PRIVATE)
        val userKey = prefs.getString("userKey", String())

        if (!userKey.isNullOrEmpty()) {
            GlobalState.userId = userKey
            GlobalScope.launch {
                GlobalState.user = UserRepository.getUser(userKey)
                runOnUiThread {
                    startActivity(ActivityTypes.MAIN)
                }
            }
        } else {
            startActivity(ActivityTypes.AUTH)
        }
    }

    private fun startActivity(activityType: ActivityTypes) {
         val intent = when (activityType) {
             ActivityTypes.MAIN -> Intent(this, MainActivity::class.java)
             ActivityTypes.AUTH -> Intent(this, AuthActivity::class.java)
         }

        startActivity(intent)
        finish()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_ACCESS_FINE_LOCATION -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    startActivity()
                }
                else {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0])) {
                        val builder = AlertDialog.Builder(this)
                        builder.setMessage(getString(R.string.set_permission_yourself))
                            .setPositiveButton(getString(R.string.clear)) { _, _ ->
                                finish()
                            }
                            .create()
                            .show()
                    }
                    else {
                        val builder = AlertDialog.Builder(this)
                        builder.setMessage(getString(R.string.about_permission_decline))
                            .setPositiveButton(getString(R.string.sure)) { _, _ ->
                                finish()
                            }
                            .setNegativeButton(getString(R.string.not_sure)) { _, _ ->
                                ActivityCompat.requestPermissions(this, permissions, requestCode)
                            }
                            .create()
                            .show()
                    }
                }
            }
        }
    }
}