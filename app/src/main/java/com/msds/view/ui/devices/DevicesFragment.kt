package com.msds.view.ui.devices

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.msds.R
import com.msds.databinding.FragmentDevicesBinding
import com.msds.view.adapter.BluetoothDeviceAdapter
import com.msds.viewmodel.DevicesViewModel

class DevicesFragment : Fragment() {
    private lateinit var devicesViewModel: DevicesViewModel
    private lateinit var binding: FragmentDevicesBinding

    private var bluetoothDeviceAdapter: BluetoothDeviceAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        devicesViewModel = ViewModelProvider(activity!!).get(DevicesViewModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_devices, container, false)

        binding.apply {
            lifecycleOwner = this@DevicesFragment
            viewModel = devicesViewModel
        }

        bluetoothDeviceAdapter = BluetoothDeviceAdapter(context!!, devicesViewModel.getDiscoveredDevices())
        binding.bluetoothDevicesListView.adapter = bluetoothDeviceAdapter

        devicesViewModel.getDiscoveredDevices().observe(binding.lifecycleOwner!!, Observer {
            bluetoothDeviceAdapter?.notifyDataSetChanged()
        })

        return binding.root
    }
}