package com.msds.view.ui.measuring

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.msds.R
import com.msds.databinding.FragmentMeasuringBinding
import com.msds.service.helper.ModeTypes
import com.msds.view.ui.GraphActivity
import com.msds.viewmodel.MeasuringViewModel

class MeasuringFragment : Fragment() {
    private lateinit var measuringViewModel: MeasuringViewModel
    private lateinit var binding: FragmentMeasuringBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        measuringViewModel = ViewModelProvider(activity!!).get(MeasuringViewModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_measuring, container, false)

        binding.apply {
            lifecycleOwner = this@MeasuringFragment
            viewModel = measuringViewModel
        }

        binding.startManometerMeasuringButton.setOnClickListener {
            startGraphActivity(ModeTypes.MANOMETER)
        }

        binding.startPulseAnalysisMeasuringButton.setOnClickListener {
            if (binding.organizationSelectionSpinner.selectedItem == null) {
                Toast.makeText(context!!, "Не выбрана ферма", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            startGraphActivity(ModeTypes.RIPPLE_ANALYSIS)
        }
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        measuringViewModel.refreshOrganizationAndFarms()

        val organizationDataAdapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_item, measuringViewModel.getOrganizationsNames())
        organizationDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.organizationSelectionSpinner.adapter = organizationDataAdapter
        binding.organizationSelectionSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) { }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val farmDataAdapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_item, measuringViewModel.getFarmsNames(binding.organizationSelectionSpinner.selectedItem.toString()))
                farmDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                binding.farmSelectionSpinner.adapter = farmDataAdapter
            }

        }
    }

    private fun startGraphActivity(modeType: ModeTypes) {
        val intent = Intent(context, GraphActivity::class.java)
        val bundle = Bundle()

        when (modeType) {
            ModeTypes.MANOMETER -> {
                bundle.putString("mode", ModeTypes.MANOMETER.toString())
            }
            ModeTypes.RIPPLE_ANALYSIS -> {
                bundle.putString("mode", ModeTypes.RIPPLE_ANALYSIS.toString())
            }
        }

        bundle.putInt("selectedOrganizationIndex", binding.organizationSelectionSpinner.selectedItemId.toInt())
        bundle.putInt("selectedFarmIndex", binding.farmSelectionSpinner.selectedItemId.toInt())
        intent.putExtras(bundle)
        startActivity(intent)
    }
}