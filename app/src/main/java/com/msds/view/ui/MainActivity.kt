package com.msds.view.ui

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.msds.R
import com.msds.service.analysis.DeviceSettings
import com.msds.service.bluetooth.BluetoothConnectionService
import com.msds.viewmodel.DevicesViewModel
import com.msds.view.listener.IBluetoothRequestEventListener
import kotlinx.android.synthetic.main.dialog_settings.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.math.roundToInt

class MainActivity : AppCompatActivity(), IBluetoothRequestEventListener {

    private val REQUEST_ENABLE_BLUEETOOTH = 101

    private lateinit var navController: NavController
    private lateinit var devicesViewModel: DevicesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        navController = findNavController(R.id.nav_host_fragment)
        val appBarConfiguration = AppBarConfiguration(setOf(
            R.id.navigation_devices,
            R.id.navigation_measuring,
            R.id.navigation_storage
        ))
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        devicesViewModel = ViewModelProvider(this).get(DevicesViewModel::class.java)
        devicesViewModel.setBluetoothEnableRequestEventListener(this)
        BluetoothConnectionService.setEventListener(devicesViewModel)

        Log.i(null, "OnCreate")
    }

    override fun onStop() {
        super.onStop()
        // BluetoothConnectionService.cleanUp()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_ENABLE_BLUEETOOTH ->  {
                if (resultCode == Activity.RESULT_OK) {
                    GlobalScope.launch {
                        delay(500)
                        runOnUiThread {
                            devicesViewModel.startDiscover()
                        }
                    }
                } else {
                    devicesViewModel.disableLoading()
                    Toast.makeText(this, "Невозможно начать поиск устройств, если Вы не включили Bluetooth", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_settings -> {
            val builder = AlertDialog.Builder(this)
            val view = LayoutInflater.from(this).inflate(R.layout.dialog_settings, null)
            view.max_vacuum_setting_edit_text.setText(DeviceSettings.VACUUM_MAX_VALUE.roundToInt().toString())
            view.border_point_setting_edit_text.setText(DeviceSettings.BORDER_POINT.roundToInt().toString())
            view.pre_buffer_size_setting_edit_text.setText(DeviceSettings.PRE_BUFFER_SIZE.toString())
            view.adc_value_first_channel_setting_edit_text.setText(DeviceSettings.ADC_VALUE_FIRST_CHANNEL.toString())
            view.adc_required_first_channel_setting_edit_text.setText(DeviceSettings.ADC_REQUIRED_FIRST_CHANNEL.toString())
            view.adc_value_second_channel_setting_edit_text.setText(DeviceSettings.ADC_VALUE_SECOND_CHANNEL.toString())
            view.adc_required_second_channel_setting_edit_text.setText(DeviceSettings.ADC_REQUIRED_SECOND_CHANNEL.toString())
            builder
                .setTitle("Настройки")
                .setView(view)
                .setPositiveButton("Сохранить") { _, _ ->
                    DeviceSettings.VACUUM_MAX_VALUE = view.max_vacuum_setting_edit_text.text.toString().toInt().toDouble()
                    DeviceSettings.BORDER_POINT = view.border_point_setting_edit_text.text.toString().toInt().toDouble()
                    DeviceSettings.PRE_BUFFER_SIZE = view.pre_buffer_size_setting_edit_text.text.toString().toInt()
                    DeviceSettings.ADC_VALUE_FIRST_CHANNEL = view.adc_value_first_channel_setting_edit_text.text.toString().toDouble()
                    DeviceSettings.ADC_REQUIRED_FIRST_CHANNEL = view.adc_required_first_channel_setting_edit_text.text.toString().toDouble()
                    DeviceSettings.ADC_VALUE_SECOND_CHANNEL = view.adc_value_second_channel_setting_edit_text.text.toString().toDouble()
                    DeviceSettings.ADC_REQUIRED_SECOND_CHANNEL = view.adc_required_second_channel_setting_edit_text.text.toString().toDouble()
                }
                .setNegativeButton("Отмена") { _, _ -> }
                .create()
                .show()
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onBluetoothEnableRequest() {
        val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BLUEETOOTH)
    }

    override fun onDeviceConnected(isSuccess: Boolean?) {
        if (isSuccess == true) {
            Toast.makeText(this, "Успешное подключение к устройству", Toast.LENGTH_LONG).show()
            navController.navigate(R.id.navigation_measuring)
        } else {
            Toast.makeText(this, "Не удалось подключиться к устройству", Toast.LENGTH_LONG).show()
        }
    }

    override fun onDeviceDisconnected() {
        Toast.makeText(this, "Соединение с устройством потеряно", Toast.LENGTH_LONG).show()
    }
}
