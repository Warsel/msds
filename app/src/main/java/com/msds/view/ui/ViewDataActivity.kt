package com.msds.view.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.msds.R
import com.msds.view.adapter.GraphPagerAdapter
import kotlinx.android.synthetic.main.activity_view_data.*

class ViewDataActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_data)

        val selectedOrganizationIndex = intent.extras!!.getInt("selectedOrganizationIndex", -1)
        val selectedFarmIndex = intent.extras!!.getInt("selectedFarmIndex", -1)
        graphViewPager.adapter = GraphPagerAdapter(supportFragmentManager, selectedOrganizationIndex, selectedFarmIndex)
    }
}