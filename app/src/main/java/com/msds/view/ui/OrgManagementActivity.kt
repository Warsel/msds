package com.msds.view.ui

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.msds.R
import com.msds.databinding.ActivityOrgManagementBinding
import com.msds.view.adapter.FarmAdapter
import com.msds.view.listener.IUIEventListener
import com.msds.viewmodel.OrgManagementViewModel

class OrgManagementActivity : AppCompatActivity(), IUIEventListener {
    private lateinit var orgManagementViewModel: OrgManagementViewModel
    private lateinit var binding: ActivityOrgManagementBinding

    private var farmAdapter: FarmAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        orgManagementViewModel = ViewModelProvider(this).get(OrgManagementViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_org_management)

        binding.apply {
            lifecycleOwner = this@OrgManagementActivity
            viewModel = orgManagementViewModel
        }

        orgManagementViewModel.setUIEventListener(this)

        farmAdapter = FarmAdapter(this, orgManagementViewModel.getFarms())
        binding.farmsListView.adapter = farmAdapter

        orgManagementViewModel.getFarms().observe(binding.lifecycleOwner!!, Observer {
            farmAdapter?.notifyDataSetChanged()
        })
    }

    override fun onStart() {
        super.onStart()
        supportActionBar!!.title = "Добавление организации"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun runAction(action: () -> Unit) {
        runOnUiThread {
            action()
        }
    }

    override fun finishActivity() {
        finish()
    }
}