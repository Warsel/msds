package com.msds.view.ui

import android.os.Bundle
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.msds.R
import com.msds.databinding.ActivityGraphBinding
import com.msds.service.analysis.DeviceSettings
import com.msds.service.bluetooth.BluetoothConnectionService
import com.msds.view.listener.IBluetoothRequestEventListener
import com.msds.view.listener.IUIEventListener
import com.msds.viewmodel.GraphViewModel

class GraphActivity : AppCompatActivity(), IBluetoothRequestEventListener, IUIEventListener {
    private lateinit var graphViewModel: GraphViewModel
    private lateinit var binding: ActivityGraphBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        val mode = intent!!.extras!!.getString("mode", null)
        val selectedOrganizationIndex = intent!!.extras!!.getInt("selectedOrganizationIndex", -1)
        val selectedFarmIndex = intent!!.extras!!.getInt("selectedFarmIndex", -1)
        graphViewModel = ViewModelProvider(this).get(GraphViewModel::class.java)
        graphViewModel.setMode(mode, selectedOrganizationIndex, selectedFarmIndex)
        graphViewModel.selectedOrganizationIndex = selectedOrganizationIndex
        graphViewModel.selectedFarmIndex = selectedFarmIndex

        binding = DataBindingUtil.setContentView(this, R.layout.activity_graph)

        binding.apply {
            lifecycleOwner = this@GraphActivity
            viewModel = graphViewModel
        }

        BluetoothConnectionService.setEventListener(graphViewModel)
        BluetoothConnectionService.setBluetoothDataReceiveEventListener(graphViewModel)
        graphViewModel.setBluetoothEnableRequestEventListener(this)
        graphViewModel.setUIEventListener(this)
        setGraphSettings()
    }

    override fun onStop() {
        super.onStop()
        graphViewModel.stopMeasuring()
    }

    private fun setGraphSettings() {
        binding.manometerGraphView.viewport.isXAxisBoundsManual = true
        binding.manometerGraphView.viewport.setMinX(0.0)
        binding.manometerGraphView.viewport.setMaxX(1250.0)

        binding.manometerGraphView.viewport.isYAxisBoundsManual = true
        binding.manometerGraphView.viewport.setMinY(0.0)
        binding.manometerGraphView.viewport.setMaxY(DeviceSettings.VACUUM_MAX_VALUE)

        graphViewModel.firstSeries.color = ContextCompat.getColor(this, R.color.colorFirstChannel)
        graphViewModel.secondSeries.color = ContextCompat.getColor(this, R.color.colorSecondChannel)

        binding.manometerGraphView.addSeries(graphViewModel.firstSeries)
        binding.manometerGraphView.addSeries(graphViewModel.secondSeries)
    }

    override fun onBluetoothEnableRequest() { }

    override fun onDeviceConnected(isSuccess: Boolean?) { }

    override fun onDeviceDisconnected() {
        Toast.makeText(this, "Соединение с устройством потеряно", Toast.LENGTH_LONG).show()
    }

    override fun runAction(action: () -> Unit) {
        runOnUiThread {
            action()
        }
    }

    override fun finishActivity() {
        finish()
    }
}