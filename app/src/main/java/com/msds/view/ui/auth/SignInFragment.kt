package com.msds.view.ui.auth

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.msds.R
import com.msds.databinding.FragmentSignInBinding
import com.msds.service.GlobalState
import com.msds.service.repository.UserRepository
import com.msds.view.ui.AuthActivity
import com.msds.viewmodel.AuthViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class SignInFragment : Fragment() {

    private lateinit var authViewModel: AuthViewModel
    private lateinit var binding: FragmentSignInBinding
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        authViewModel = ViewModelProvider(activity!!).get(AuthViewModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_in, container, false)
        binding.apply {
            lifecycleOwner = this@SignInFragment
            viewmodel = authViewModel
        }

        binding.signInButton.setOnClickListener {
            authViewModel.enableLoading()
            GlobalScope.launch {
                val isUserExist = authViewModel.signIn()

                if (isUserExist) {
                    GlobalState.userId = authViewModel.userKey.value!!
                    GlobalState.user = UserRepository.getUser(GlobalState.userId!!)

                    (activity as AuthActivity).apply {
                        saveUserKeyToPreference(authViewModel.userKey.value!!)
                        startMainActivity()
                    }
                } else {
                    activity!!.runOnUiThread {
                        authViewModel.disableLoading()
                    }
                }
            }
        }

        binding.signUpButton.setOnClickListener {
            authViewModel.enableLoading()
            authViewModel.generateUserKey()

            GlobalScope.launch {
                authViewModel.signUp()

                activity!!.runOnUiThread {
                    navController.navigate(R.id.action_signInFragment_to_signUpFragment)
                }
            }
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = findNavController()
    }
}