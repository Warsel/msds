package com.msds.view.ui.storage

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.msds.R
import com.msds.databinding.FragmentStorageBinding
import com.msds.service.GlobalState
import com.msds.view.adapter.OrganizationAdapter
import com.msds.view.listener.IStorageEventListener
import com.msds.view.ui.OrgManagementActivity
import com.msds.viewmodel.StorageViewModel
import com.msds.view.ui.ViewDataActivity

class StorageFragment : Fragment(), IStorageEventListener {
    private lateinit var storageViewModel: StorageViewModel
    private lateinit var binding: FragmentStorageBinding

    private var organizationAdapter: OrganizationAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        storageViewModel = ViewModelProvider(activity!!).get(StorageViewModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_storage, container, false)

        binding.apply {
            lifecycleOwner = this@StorageFragment
            viewModel = storageViewModel
        }

        storageViewModel.setEventListener(this)

        organizationAdapter = OrganizationAdapter(context!!, storageViewModel.getOrganizations().value!!)
        binding.organizationsExpandableListView.setAdapter(organizationAdapter)

        storageViewModel.getOrganizations().observe(binding.lifecycleOwner!!, Observer {
            organizationAdapter?.notifyDataSetChanged()
        })

        binding.organizationsExpandableListView.setOnChildClickListener { _, _, groupPosition, childPosition, _ ->
            startViewDataActivity(groupPosition, childPosition)
            true
        }

        return binding.root
    }

    override fun onAddNewOrganization() {
        val intent = Intent(context!!, OrgManagementActivity::class.java)
        startActivity(intent)
    }

    private fun startViewDataActivity(selectedOrganizationIndex: Int, selectedFarmIndex: Int) {
        val bundle = Bundle()
        bundle.putInt("selectedOrganizationIndex", selectedOrganizationIndex)
        bundle.putInt("selectedFarmIndex", selectedFarmIndex)

        if (GlobalState.user!!.organizations[selectedOrganizationIndex].farms[selectedFarmIndex].workplaces.size == 0) {
            Toast.makeText(context!!, "Нет сохранённых данных", Toast.LENGTH_LONG).show()
            return
        }
        
        val intent = Intent(activity!!, ViewDataActivity::class.java)
        intent.putExtras(bundle)

        startActivity(intent)
    }
}