package com.msds.view.listener

interface IBluetoothRequestEventListener {
    fun onBluetoothEnableRequest()
    fun onDeviceConnected(isSuccess: Boolean?)
    fun onDeviceDisconnected()
}