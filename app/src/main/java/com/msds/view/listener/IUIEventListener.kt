package com.msds.view.listener

interface IUIEventListener {
    fun runAction(action: () -> Unit)
    fun finishActivity()
}